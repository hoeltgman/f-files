! mod_OC_inp, a fortran module for optimal control inpainting.
! Copyright (C) 2014 Laurent Hoeltgen <hoeltgen@mia.uni-saarland.de>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.

program OC
!    use :: iso_c_binding
!    use :: netpbm
!    use :: oc_inp
!    use :: math_fun
!    implicit none
!
!    integer                                           :: argc = 0        ! Number of command line parameters.
!    integer                                           :: arglen = 0      ! Length of command line parameter.
!    integer                                           :: status = 0      ! status if command was successful.
!    integer(kind=c_long)                              :: nr = -1         ! number of rows of input image.
!    integer(kind=c_long)                              :: nc = -1         ! number of columns of input image.
!    integer(kind=c_long)                              :: nd = -1         ! number of channels of input image.
!    integer(kind=c_long)                              :: mv = -1         ! theoretically highest colour value.
!    integer(kind=c_long)                              :: maxitI = -1     ! Number of inner iterations.
!    integer(kind=c_long)                              :: maxitO = -1     ! Number of outer iterations.
!    integer(kind=c_long)                              :: gvoits = -1     ! Number of GVO iterations.
!    integer(kind=c_long)                              :: gvometh = 1     ! GVO method to be used.
!    character(len=64)                                 :: buf             ! Temporary buffer.
!    character(len=64)                                 :: buf2            ! Temporary buffer.
!    character(len=64)                                 :: img             ! Input image name.
!    character(len=64)                                 :: outu            ! Output image name.
!    character(len=64)                                 :: outc            ! Output mask name.
!    character                                         :: method = 'p'    ! Whether to employ the primal or dual method.
!    real(kind=c_double)                               :: lambda = -1.0D0 ! Sparsity parameter.
!    real(kind=c_double)                               :: epsi = -1.0D0   ! Epsilon regularisation.
!    real(kind=c_double)                               :: mu = -1.0D0     ! Weight of the proximal term.
!    real(kind=c_double), dimension(:, :), allocatable :: pixel           ! Input image data.
!    real(kind=c_double), dimension(:, :), allocatable :: u               ! Output image data.
!    real(kind=c_double), dimension(:, :), allocatable :: c               ! Output mask data.
!    real(kind=c_double), dimension(:, :), allocatable :: tmp             ! Tempory buffer for img data.
!    real(kind=c_double), dimension(:, :), allocatable :: cbin            ! Binarised output mask.
!    character(len=64)                                 :: fmt             ! Format string for I/O.
!
!
!    write (*,*) "Compute optimal mask positions for Laplace interpolation (grayscale version)."
!    write (*,*) "Copyright (C) 2014 Laurent Hoeltgen <hoeltgen@mia.uni-saarland.de>"
!    write (*,*) ""
!    write (*,*) "This program is free software: you can redistribute it and/or modify"
!    write (*,*) "it under the terms of the GNU General Public License as published by"
!    write (*,*) "the Free Software Foundation, either version 3 of the License, or"
!    write (*,*) "(at your option) any later version."
!    write (*,*) ""
!    write (*,*) "This program is distributed in the hope that it will be useful,"
!    write (*,*) "but WITHOUT ANY WARRANTY; without even the implied warranty of"
!    write (*,*) "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
!    write (*,*) "GNU General Public License for more details."
!    write (*,*) ""
!    write (*,*) "You should have received a copy of the GNU General Public License"
!    write (*,*) "along with this program.  If not, see <http://www.gnu.org/licenses/>."
!    write (*,*) ""
!    write (*,*) ""
!
!    ! Get number of command line parameters.
!    argc = command_argument_count()
!    if (argc /= 11) then
!       write (*,*) "Usage:"
!       write (*,*) "OC <img> <p|d> <lam> <eps> <mu> <maxitI> <maxitO> <oimg> <omask> <gvoits> <gvometh>"
!       write (*,*) ""
!       write (*,*) "<img>:     Input image (pgm file)."
!       write (*,*) "<p|d>:     Whether to use a primal or dual solver."
!       write (*,*) "<lam>:     Sparsity parameter."
!       write (*,*) "<eps>:     Regularisation weight."
!       write (*,*) "<mu>:      Proximal weight."
!       write (*,*) "<maxitI>:  Number of Linearisations."
!       write (*,*) "<maxitO>:  Number of iterations used to solve the linearised problem."
!       write (*,*) "<oimg>:    Output image (pgm file)."
!       write (*,*) "<omask>:   Output mask (pgm file)."
!       write (*,*) "<gvoits>:  Number of iterations for gray value optimisation."
!       write (*,*) "<gvometh>: Method to be used for GVO. (1: lsqr, 2: rrlsqr, 3: pdhg, 4: pdhg/umfpack)"
!       write (*,*) ""
!       stop
!    else
!       write (*,*) "Program parameters:"
!       write (*,*) ""
!       call get_command_argument(0, buf, arglen, status)
!       write (fmt,*) '(1X,A19,11X,A', arglen, ')'
!       write (*,fmt) "Name of execuatble:", trim(buf(1:arglen))
!
!       call get_command_argument(1, buf, arglen, status)
!       read (buf,*) img
!       write (fmt,*) '(1X,A12,18X,A', arglen, ')'
!       write (*,fmt) "Input image:", trim(img)
!
!       call get_command_argument(2, buf, arglen, status)
!       read (buf,'(A1)') method
!       if (method == 'p') then
!          write (*,'(1X,A7,23X,A6)') "Solver:","Primal"
!       else
!          write (*,'(1X,A6,24X,A4)') "Solver","Dual"
!       end if
!
!       call get_command_argument(3, buf, arglen, status)
!       read (buf(1:arglen),'(F9.6)') lambda
!       write (buf,'(ES15.6)') lambda
!       write (fmt,*) '(1X,A7,23X,A15)'
!       write (*,fmt) "Lambda:", adjustl(trim(buf))
!
!       call get_command_argument(4, buf, arglen, status)
!       read (buf(1:arglen),'(F9.6)') epsi
!       write (buf,'(ES15.6)') epsi
!       write (fmt,*) '(1X,A8,22X,A15)'
!       write (*,fmt) "Epsilon:", adjustl(trim(buf))
!
!       call get_command_argument(5, buf, arglen, status)
!       read (buf(1:arglen),'(F9.6)') mu
!       write (buf,'(ES15.6)') mu
!       write (fmt,*) '(1X,A3,27X,A15)'
!       write (*,fmt) "Mu:", adjustl(trim(buf))
!
!       call get_command_argument(6, buf, arglen, status)
!       read (buf(1:arglen),'(i10)') maxitI
!       write (fmt,*) '(1X,A17,13X,I9.9)'
!       write (*,fmt) "Inner iterations:", maxitI
!
!       call get_command_argument(7, buf, arglen, status)
!       write (fmt,*) '(1X,A17,13X,I9.9)'
!       read (buf(1:arglen),'(i10)') maxitO
!       write (*,fmt) "Outer iterations:", maxitO
!
!       call get_command_argument(8, buf, arglen, status)
!       read (buf(1:arglen),*) outu
!       write (fmt,*) '(1X,A27,3X,A', arglen, ')'
!       write (*,fmt) "Output file reconstruction:", trim(outu)
!
!       call get_command_argument(9, buf, arglen, status)
!       read (buf(1:arglen),*) outc
!       write (fmt,*) '(1X,A17,13X,A', arglen, ')'
!       write (*,fmt) "Output file mask:", trim(outc)
!
!       call get_command_argument(10, buf, arglen, status)
!       read (buf(1:arglen),'(i10)') gvoits
!       write (fmt,*) '(1X,A15,15X,I9.9)'
!       write (*,fmt) "GVO iterations:", gvoits
!
!       call get_command_argument(11, buf, arglen, status)
!       read (buf(1:arglen),'(i10)') gvometh
!       select case (gvometh)
!       case (1)
!          write (*,'(1X,A11,19X,A4)') "GVO method:", "LSQR"
!       case (2)
!          write (*,'(1X,A11,19X,A6)') "GVO method:", "RRLSQR"
!       case (3)
!          write (*,'(1X,A11,19X,A4)') "GVO method:", "PDHG"
!       case (4)
!          write (*,'(1X,A11,19X,A12)') "GVO method:", "PDHG/UMFPACK"
!       case (5)
!          write (*,'(1X,A11,19X,A12)') "GVO method:", "Damped LSQR"
!       case default
!          write (*,'(1X,A11,19X,A16)') "GVO method:", "Fallback to PDHG"
!          gvometh = int(3, c_long)
!       end select
!    end if
!
!    write (*,*) ""
!    write (*,*) "Reading image size."
!    write (*,*) ""
!    call get_image_dimensions (trim(img), nr, nc, nd, mv)
!
!    write (*,'(1X,A17,13X,I4.4,1X,A1,1X,I4.4)') "Image dimensions:", nr, "x", nc
!    write (*,'(1X,A19,11X,I1.1)') "Number of channels:", nd
!    write (*,'(1X,A10,20X,I5.5)') "Max Value:", mv
!
!    write (*,*) ""
!    write (*,*) "Allocating memory."
!    write (*,*) ""
!    allocate(pixel(nr,nc), u(nr,nc), c(nr,nc), cbin(nr,nc), tmp(nr,nc))
!
!    write (*,*) "Reading image data."
!    write (*,*) ""
!    !! Todo I could merge OC and OC3 now into 1 program.
!    call get_image_data (trim(img), nr, nc, nd, mv, pixel)
!
!    write (buf, '(F9.3)') minval(pixel)
!    write (buf2,'(F9.3)') maxval(pixel)
!    write (*,'(1X,A11,19X,A9,A9)') "Data range:", adjustl(trim(buf)), adjustl(trim(buf2))
!    write (*,*) ""
!    pixel = pixel/real(mv, c_double)
!    write (*,*) "Computing optimal mask."
!    write (*,*) ""
!    if (method == 'p') then
!       call solve_OC_pdhg(nr, nc, lambda, epsi, mu, pixel, maxitI, maxitO, u, c)
!    else
!       call solve_OC_dual(nr, nc, lambda, epsi, mu, pixel, maxitI, maxitO, u, c)
!    end if
!
!    write (buf,'(F9.3)') energy(nr, nc, u, c, pixel, u, c, lambda, epsi, 0.0D0)
!    write (*,'(1X,A22,8X,A9)') "Energy (primal) value:", adjustl(trim(buf))
!
!    tmp = 0.0D0
!    call eval_pde(nr, nc, u, c, pixel, tmp)
!    write (buf,'(F9.3)') nrmf(tmp)
!    write (*,'(1X,A12,18X,A9)') "Residual PDE", adjustl(trim(buf))
!    tmp = 0.0D0
!
!    write (buf,'(F9.3)') mse_error(nr, nc, u, pixel, real(mv, c_double))
!    write (*,'(1X,A13,17X,A9)') "MSE Solution:", adjustl(trim(buf))
!
!    u = real(mv, c_double)*u
!    c = real(mv, c_double)*c
!
!    write (buf,'(F9.3)')  minval(u)
!    write (buf2,'(F9.3)') maxval(u)
!    write (*,'(1X,A26,4X,A9,A9)') "Data range reconstruction:", adjustl(trim(buf)), adjustl(trim(buf2))
!    write (buf,'(F9.3)')  minval(c)
!    write (buf2,'(F9.3)') maxval(c)
!    write (*,'(1X,A16,14X,A9,A9)') "Data range mask:", adjustl(trim(buf)), adjustl(trim(buf2))
!    Write (*,*) ""
!
!    write (*,'(1X,A24)') "Writing result to files."
!    write (*,*) ""
!    call write_image(trim(outu), nr, nc, nd, mv, u)
!    call write_image(trim(outc), nr, nc, nd, mv, c)
!
!    ! We threshold at 0.01 (c somewhere in the range of [0,1])
!    call binarise(nr, nc, abs(c), 0.01D0, cbin)
!
!    write (buf,'(F9.3)') 100.0D0*(sum(cbin)/real(nr*nc, c_double))
!    write (*,'(1X,A24,6X,A9)') "Density of the mask (%):", adjustl(trim(buf))
!
!    u = 0.0D0
!
!    write (*,*) ""
!    write (*,*) "Solving with binary mask."
!    write (*,*) ""
!
!    call solve_inpainting(nr, nc, cbin, cbin*pixel, .false., u, int(2, c_long))
!
!    write (buf,'(F9.3)')  minval(u)*real(mv, c_double)
!    write (buf2,'(F9.3)') maxval(u)*real(mv, c_double)
!    write (*,'(1X,A20,10X,A9,A9)') "Data range solution:", adjustl(trim(buf)), adjustl(trim(buf2))
!    write (buf,'(F9.3)') mse_error(nr, nc, u, pixel, real(mv, c_double))
!    write (*,'(1X,A20,10X,A9)') "MSE binary solution:", adjustl(trim(buf))
!    write (*,*) ""
!
!    u = real(mv, c_double)*u
!
!    arglen = len_trim(outc)
!    write (buf,*) outc(1:(arglen-4)), "-bin.pgm"
!
!    write (fmt,*) '(1X,A23,7X,A', len_trim(buf), ')'
!    write (*,fmt) "Writing binary mask to:", adjustl(trim(buf))
!
!    call write_image(adjustl(trim(buf)), nr, nc, nd, mv, real(mv, c_double)*cbin)
!
!    arglen = len_trim(outu)
!    write (buf,*) outu(1:(arglen-4)), "-bin.pgm"
!
!    write (fmt,*) '(1X,A27,3X,A', len_trim(buf), ')'
!    write (*,fmt) "Writing binary solution to:", adjustl(trim(buf))
!
!    call write_image(adjustl(trim(buf)), nr, nc, nd, mv, u)
!
!    if (gvoits > 0) then
!
!       write (*,*) ""
!       write (*,*) "Performing GVO with binary mask."
!       write (*,*) ""
!       u = 0.0D0
!       call apply_gvo(nr, nc, gvoits, cbin, pixel, tmp, gvometh)
!       call solve_inpainting(nr, nc, cbin, cbin*tmp, .false., u, int(2, c_long))
!
!       write (buf,'(F9.3)')  minval(u)*real(mv, c_double)
!       write (buf2,'(F9.3)') maxval(u)*real(mv, c_double)
!       write (*,'(1X,A24,6X,A9,A9)') "Data range GVO solution:", adjustl(trim(buf)), adjustl(trim(buf2))
!       write (buf,'(F9.3)')  mse_error(nr, nc, u, pixel, real(mv, c_double))
!       write (*,'(1X,A17,13X,A9)') "MSE GVO solution:", adjustl(trim(buf))
!
!       arglen = len_trim(outu)
!       write (buf,*) outu(1:(arglen-4)), "-gvo.pgm"
!
!       u = u*real(mv, c_double)
!       write (*,*) ""
!       write (fmt,*) '(1X,A24,6X,A', len_trim(buf), ')'
!       write (*,fmt) "Writing gvo solution to:", adjustl(trim(buf))
!       call write_image(trim(buf), nr, nc, nd, mv, u)
!
!    end if
!
!    write (*,*) ""
!    write (*,*) "Deallocating Memory."
!    deallocate(pixel, cbin, u, c, tmp)

    write (*,'(/,1X,A5,/)') "Done."
end program OC
