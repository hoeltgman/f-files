include makefile.defs

all:
	$(MAKE) --directory=modules all
	$(MAKE) --directory=tests all
	$(MAKE) --directory=bin all

clean:
	$(RM) *.o *.mod *.dyn *.spi *.spl *.a \#*
	$(MAKE) --directory=tests clean
	$(MAKE) --directory=modules clean

doc: $(FSRC)
	robodoc --src ./modules --doc ./documentation --sections --toc --syntaxcolors --source_line_numbers --multidoc --index --html

tags: $(FSRC) $(CSRC)
	ctags-exuberant -e --langmap=fortran:.F08 $^

test:
	$(MAKE) --directory=tests all

%.o : %.F08
	$(FC) $(FCFLAGS) -c $<

%.o : %.c
	$(CC) $(CCFLAGS) -c $<
